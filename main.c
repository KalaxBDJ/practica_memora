#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <conio.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

#define void descriptores(char v[], int N , char* D1[]);

	bool reservadoD[5];

int main(int argc, char *argv[]) {
	int N = 10;
	int descriptores;
	char RAM[N];
	char VIRTUAL[N];
	char* D[N];
	char* DV[N];
	
	int op2;
	
	char cTecla;
	
	int opc;
	bool exit = false;
	
	
	char* y[N];
	char ss;
	
	printf("Inicializando RAM...\n\n");
	for(int i = 0 ; i < N ; i++)
	{
		RAM[i] = '0';
		VIRTUAL[i]='0';
		DV[i]=&VIRTUAL[i];
	}
	
	/* Pointer example */
	/*
	printf("Value: %c\n",*y[3]);
	printf("Location: %u\n",y[3]);
	*/
	
	printf("Asignando descriptores y localizacion en RAM ...\n");
	
	
	printf("\n\n-------------------------------------------------------------\n");
	adescriptores(RAM,N,D);
	
	printf("\n");
	
	do
	{
		
		printf("Ingrese el numero con la opcion que desee realizar: \n");
		printf("\n      *MENU*\n1-RESERVAR MEMORIA\n2-ESCRIBIR MEMORIA\n3-LEER MEMORIA\n4-LIBERAR MEMORIA\n5-LLEVAR A VIRTUAL\n6-TRAER A RAM\n7-MOSTRAR RAM\n8-MOSTRAR VIRTUAL\n9-SALIR\n\n");
		scanf("%i",&opc);
		switch(opc)
		{
			case 1:
				/* RESERVAR MEMORIA */
				printf("Ingrese el numero de descriptor que desea reservar (1-5) : ");
				scanf("%i",&op2);
				reservadoD[op2-1] = true;
				printf("\n�|DESCRIPTOR NUMERO %i RESERVADO|�",op2);
				printf("\n");
				
				break;
			case 2:
				/* ESCRIBIR MEMORIA*/
				printf("Ingrese el numero de descriptor en el que desea escribir (1-5) : ");
				scanf("%i",&op2);
				printf("\n");
				escribirMemoria(op2,D);
				break;
			case 3:
				/* LEER MEMORIA */
				printf("Ingrese el numero de descriptor el cual desea leer (1-5) : ");
				scanf("%i",&op2);
				printf("\n");
				leerMemoria(op2,D);
				break;
			case 4:
				/* LIBERAR MEMORIA */
				printf("Ingrese el numero de descriptor que desea liberar (1-5) : ");
				scanf("%i",&op2);
				reservadoD[op2-1] = false;
				printf("\n�|DESCRIPTOR NUMERO %i LIBERADO|�",op2);
				printf("\n");
				break;
			case 5:
				/* LLEVAR A VIRTUAL */
				printf("Ingrese el numero de descriptor que desea llevar a virtual (1-5) : ");
				scanf("%i",&op2);
				llevarVirtual(D,DV,op2);
				break;
			case 6:
				/* TRAER A RAM */
				printf("Ingrese el numero de descriptor que desea llevar a virtual (1-5) : ");
				scanf("%i",&op2);
				llevarRAM(D,DV,op2);
				break;
			case 7:
				/* MOSTRAR RAM */
				mostrarRAM(RAM,"RAM");
				break;
			case 8:
				/* MOSTRAR VIRTUAL */
				mostrarRAM(VIRTUAL,"VIRTUAL");
				break;
			case 9:
				/* SALIR */
				printf("Ha salido del menu\n");
				exit = true;
				break;
			default:
				printf("Fuera de Rango :c\n");
				break;
		}
		printf("Presione Enter.\n");
		cTecla = getch();
		system("cls");
	}
	while(!exit);
	
	printf("\n\n�|CODED BY: KALAX|�");
	return 0;
}

void llevarRAM(char* r[],char* v[],int descriptor)
{
	int numd = descriptor - 1 ;
	descriptor = (descriptor * 2) - 2 ;
	
	for(int i = 0 ; i < 2 ; i++)
	{
		if(*v[descriptor] == '0')
		{
			printf("�|ERROR:NO SE PUDO LLEVAR A RAM|�\n");
			printf("�|ERROR:NO HAY NINGUNA TAREA EJECUTANDOSE EN ESTA POSICION EN VIRTUAL|�\n");
			break;
		}
		else if(reservadoD[numd] == true)
		{
			printf("\n�|ERROR:LOS ESPACIOS EN MEMORIA SE ENCUENTRAN RESERVADOS|�\n");
			break;
		}
		else if(*r[descriptor] == '0' && reservadoD[numd] == false)
		{
			printf("\n�|OK:PASANDO DE VIRTUAL A RAM|�\n");
			
			*r[descriptor] = *v[descriptor];
			*v[descriptor] = '0';
			
		}
		else if(*r[descriptor] != '0' && reservadoD[numd] == false)
		{
			printf("\n�|OK:PASANDO DE VIRTUAL A RAM|�\n");
			printf("\n�|WARNING:EL ESPACIO EN MEMORIA SE ENCONTRABA OCUPADO, LOS VALORES SE SOBREESCRIBIERON|�\n");
			
			*r[descriptor] = *v[descriptor];
			*v[descriptor] = '0';
		}
		
		descriptor++;
	}
	
}

void llevarVirtual(char* r[],char* v[],int descriptor)
{
	int numd = descriptor - 1 ;
	descriptor = (descriptor * 2) - 2 ;
	
	for(int i = 0 ; i < 2 ; i++)
	{
		if(*v[descriptor] != '0')
		{
			printf("\n�|YA ESAS POSICIONES EN MEMORIA VIRTUAL SE ENCUENTRAN OCUPADAS|�\n�|ERROR:NO SE PUDO LLEVAR A VIRTUAL|�\n");
			break;
		}
		else if(*r[descriptor] == '0')
		{
			printf("�|ERROR:NO SE PUDO LLEVAR A VIRTUAL|�\n");
			printf("�|ERROR:NO HAY NINGUNA TAREA EJECUTANDOSE EN ESTA POSICION|�\n");
			break;
		}
		else
		{
			*v[descriptor] = *r[descriptor];
			*r[descriptor] = '0';
			
			if(reservadoD[numd] == true)
			{
				reservadoD[numd] = false;
				printf("�|OK�\n");
				printf("�|DESCRIPTOR NUMERO %i LIBERADO|�\n",numd+1);
				
			}
			
		}
		descriptor++;
	}
	
}


void leerMemoria(int descriptor, char* D[])
{
	int desc = descriptor;
	descriptor = (descriptor * 2) - 2 ;
	for(int i = 0 ; i < 2 ; i++)
	{
		printf("\n�|DESCRIPTOR #%i EN LA POSICION NUMERO %i : %c|�\n",desc,descriptor,*D[descriptor]);	
		descriptor++;	
	}
	
}


void escribirMemoria(int descriptor, char* D[])
{	
	if(!reservadoD[descriptor-1])
	{
	
		char text[2];
		printf("Escriba lo que desee guardar en memoria: ");
		scanf("%s",&text);
		printf("\n\nGuardando en memoria : %s\n\n",text);
		
		descriptor = (descriptor * 2) - 2 ;
		
		for(int i = 0 ; i < 2 ; i++)
		{
			*D[descriptor] = text[i];
			descriptor++;
		}
		
	}
	else
	{
		printf("\n�|ERROR:El espacio en memoria esta reservado|�\n");
	}
	
}

void mostrarRAM(char v[],char text[10])
{
	printf(	"\n           %s         \n=========================\n   0        1        2   \n|  %c  |  |  %c  |  |  %c  |\n=========================\n   3        4        5   \n|  %c  |  |  %c  |  |  %c  |\n=========================\n   6        7        8   \n|  %c  |  |  %c  |  |  %c  |\n=========================\n            9            \n         |  %c  |          \n=========================\n",text,v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9]);
	
}

void adescriptores(char v[], int N , char* D[])
{
	char temp = 's';
	int tempo = 1;
	int pasos = 0;
	
	for( int i =0 ; i < N ; i++)
	{
		if(pasos == 2)
		{
			printf("\n-------------------------------------------------------------\n");
			tempo++;
			pasos = 0;
		}
		
		D[i] = &v[i];
		printf("Posicion de memoria RAM asignada para el descriptor %i : %i\n",tempo,i);
		pasos++;
	}
	
}



